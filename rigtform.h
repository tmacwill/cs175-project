#ifndef RIGTFORM_H
#define RIGTFORM_H

#include <iostream>
#include <cassert>

#include "matrix4.h"
#include "quat.h"

using namespace std;      // for string, vector, iostream, and other standard C++ stuff

class RigTForm {
    Cvec3 t_; // translation component
    Quat r_;  // rotation component represented as a quaternion

    public:
    RigTForm() : t_(0) {
        assert(norm2(Quat(1,0,0,0) - r_) < CS175_EPS2);
    }

    RigTForm(const Cvec3& t, const Quat& r) {
        t_ = t;
        r_ = r;
    }

    RigTForm(const Cvec3& t) {
        r_ = Quat(1, 0, 0, 0);
        t_ = t;
    }

    RigTForm(const Quat& r) {
        r_ = r;
        t_ = Cvec3(0, 0, 0);
    }

    Cvec3 getTranslation() const {
        return t_;
    }

    Quat getRotation() const {
        return r_;
    }

    RigTForm& setTranslation(const Cvec3& t) {
        t_ = t;
        return *this;
    }

    RigTForm& setRotation(const Quat& r) {
        r_ = r;
        return *this;
    }

    Cvec4 operator * (const Cvec4& a) const {
        // no translation if 4th coordinate is 0
        if (a[3] == 0)
            return this->getRotation() * a;

        return this->getRotation() * a + Cvec4(this->getTranslation(), 0);
    }

    RigTForm operator * (const RigTForm& a) const {
        Quat r1 = this->getRotation();
        Quat r2 = a.getRotation();

        Cvec3 t1 = this->getTranslation();
        Cvec3 t2 = a.getTranslation();

        return RigTForm(Cvec3(Cvec4(t1, 0) + r1 * Cvec4(t2, 0)), r1 * r2);
    }
};

inline RigTForm inv(const RigTForm& tform) {
    Quat r = inv(tform.getRotation());
    Cvec3 t = tform.getTranslation();

    return RigTForm(-Cvec3(r * Cvec4(t, 0)), r);
}

inline RigTForm transFact(const RigTForm& tform) {
    return RigTForm(tform.getTranslation());
}

inline RigTForm linFact(const RigTForm& tform) {
    return RigTForm(tform.getRotation());
}

inline Matrix4 rigTFormToMatrix(const RigTForm& tform) {
    Matrix4 T = Matrix4::makeTranslation(tform.getTranslation());
    Matrix4 R = quatToMatrix(tform.getRotation());
    return T * R;
}

inline RigTForm doMtoOwrtA(const RigTForm& m, const RigTForm& o, const RigTForm& a) {
    return a * m * inv(a) * o;
}

inline Cvec3 lerp(Cvec3 c0, Cvec3 c1, double alpha) {
    return (c0 * (1.0 - alpha)) + (c1 * alpha);
}

inline Quat slerp(Quat q0, Quat q1, double alpha) {
    Quat q = q1 * inv(q0);
    if (q[0] < 0)
        q = Quat(-q[0], -q[1], -q[2], -q[3]);

    return pow(q, alpha) * q0;
}

inline Cvec3 catmullRomSpline(Cvec3 c0, Cvec3 c1, Cvec3 c2, Cvec3 c3, double alpha) {
    Cvec3 d = (c2 - c0)/6.0 + c1;
    Cvec3 e = (c1 - c3)/6.0 + c2;

    Cvec3 f = lerp(c1, d, alpha);
    Cvec3 g = lerp(d, e, alpha);
    Cvec3 h = lerp(e, c2, alpha);
    Cvec3 m = lerp(f, g, alpha);
    Cvec3 n = lerp(g, h, alpha);
    return lerp(m, n, alpha);
}

inline Quat quatSpline(Quat q0, Quat q1, Quat q2, Quat q3, double alpha) {
    Quat q = q2 * inv(q0);
    if (q[0] < 0)
        q = Quat(-q[0], -q[1], -q[2], -q[3]);
    Quat d = pow(q, 1/6.0) * q1;


    q = q3 * inv(q1);
    if (q[0] < 0)
        q = Quat(-q[0], -q[1], -q[2], -q[3]);
    Quat e = pow(q, -1/6.0) * q2;

    Quat f = slerp(q1, d, alpha);
    Quat g = slerp(d, e, alpha);
    Quat h = slerp(e, q2, alpha);
    Quat m = slerp(f, g, alpha);
    Quat n = slerp(g, h, alpha);
    return slerp(m, n, alpha);
}

#endif
