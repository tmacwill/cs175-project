#include <fstream>
#include <vector>
#include <map>
#include <utility>
#include <stdlib.h>
#include <vector>
#include <iostream>

#include "cvec.h"

#include <GL/glew.h>
#ifdef __MAC__
#   include <GLUT/glut.h>
#else
#   include <GL/glut.h>
#endif

using namespace std;

class SpringParticle {
private:
    Cvec3 position;
    Cvec3 velocity;
    Cvec3 color;

    Cvec3 center;
    // mass of the center of the system
    float centerMass;
    
    // mass of this particle
    float mass;

    Cvec3 newPosition;

public:
    SpringParticle(Cvec3 cent, float centerM, Cvec3 pos, float thisM) {
        position = pos;
        mass = thisM;

        center = cent;
        centerMass = centerM;

        velocity = Cvec3(0.0, 0.0, 0.0);
    }

    void setColor(Cvec3 c) { color = c; }

    Cvec3 getPosition() { return this->position; }
    Cvec3 getColor() { return color; }

    float getAlpha() {
        return 1;
    }

    float getMass() {
        return mass;
    }

    // updates the particles position and velocity
    // particles = list of all particles in the system
    // speed = speed at which particles move (the spring distance between them)
    // scale = relative size of the overall system
    void update(vector<SpringParticle> particles, float speed, float scale) {
        Cvec3 acc(0.0, 0.0, 0.0);

        for (int i = 0; i < particles.size(); i++) {
            Cvec3 otherPos = particles[i].getPosition();
            if (otherPos[0] != position[0] || otherPos[1] != position[1] || otherPos[2] != position[2]) {
                Cvec3 diff = position - otherPos;
                diff /= scale;
                float dist = diff[0]*diff[0] + diff[1]*diff[1] + diff[2]*diff[2];
                acc += diff * (speed-dist);
            }
        }


        // cap acceleration at 0.1 in any direction
        /**for (int i = 0; i < 3; i++)
        {
            if (acc[i] > 0.1)
                acc[i] = 0.1;
            else if (acc[i] < -0.1)
                acc[i] = -0.1;
        }*/

        acc -= velocity * 0.01;

        velocity += acc;
        newPosition = position + velocity;
    }

    void update2() {
        position = Cvec3(newPosition);
    }

    // Reset the center of mass
    void resetCM(Cvec3 cm) {
        position += center - cm;
    }
};


class ParticleCloud {
    // point from which particles originate
    Cvec3 source;

    // particles initial direction and speed
    Cvec3 direction;

    // some randomness in particles' initial velocity
    Cvec3 randomness;

    // net forces acting on all particles, in each (xyz) direction
    Cvec3 forces;

    // color of new particles
    Cvec3 color;

    // all particles
    vector<SpringParticle> particles;

    int numParticles;
    int spawnRate;
    int maxParticles;
    int lifespan;
    bool fade;

    float speed;
    float scale;
    
public:
    ParticleCloud(Cvec3 s, int numP, Cvec3 c) {
        source = s;
        numParticles = numP;

        // default color is white
        color = c;
        
        reset();
    }

    void reset() {
        particles.clear();

        speed = 0.001;
        scale = 10;

        for (int i = 0; i < numParticles; i++) {
            particles.push_back(createNew());
        }
    }

    SpringParticle createNew() {
        //float mass = (double)rand() / RAND_MAX;
        //mass /= 10000;

        // start at a small random offset from source
        Cvec3 offset(0.0, 0.0, 0.0);
        for (int i = 0; i < 3; i++) {
            float d = (double)rand() / RAND_MAX - 0.5;
            d /= 100;
            offset[i] += d;
        }

        SpringParticle p(source, 0.001, source+offset, 0.00001);
        p.setColor(color);
        return p;
    }

    void update() {
        if (!stable())
            reset();


        Cvec3 cm(0.0, 0.0, 0.0);
        // update existing particles
        for (int i = 0; i < numParticles; i++) {
            particles[i].update(particles, speed, scale);
        }
        for (int i = 0; i < numParticles; i++) {
            particles[i].update2();
            cm += particles[i].getPosition();
        }
        
        cm /= numParticles;

        for (int i = 0; i < numParticles; i++) {
            particles[i].resetCM(cm);
        }
    }
 
    // returns true if the system is stable
    // false if the system has exploded (if a majority of the points are beyond the field of view)
    bool stable() {
        int numNear = 0;
        for (int i = 0; i < particles.size(); i++) {
            Cvec3 diff = particles[i].getPosition() - source;
            float dist = diff[0]*diff[0] + diff[1]*diff[1] + diff[2]*diff[2];
            if (dist < 1) {
                return true;
            }
        }
        return false;
    }

    vector<SpringParticle> getParticles() {
        return particles;
    }

    void setColor(Cvec3 c) {
        color = c;
        for (int i = 0; i < particles.size(); i++)
            particles[i].setColor(color);
    }

    void setRate(int r) {
        spawnRate = r;
    }

    void setLifespan(int l) {
        lifespan = l;
    }

    void setSpeed(float s) {
        speed = s;
    }

    void setScale(float s) {
        scale = s;
    }
};
