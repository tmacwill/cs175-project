LIB_PATH            = lib/lib
LIB_NAME_RELEASE    = libfmodex.dylib
LIB_NAME_LOGGING    = libfmodexL.dylib
ARCH                = -arch i386

OS := $(shell uname -s)

ifeq ($(OS), Linux) # Science Center Linux Boxes
  CPPFLAGS = -I/home/l/i/lib175/usr/glew/include
  LDFLAGS += -L/home/l/i/lib175/usr/glew/lib -L/usr/X11R6/lib
  LIBS += -lGL -lGLU -lglut
endif

ifeq ($(OS), Darwin) # Assume OS X
  CPPFLAGS += -D__MAC__
  LDFLAGS += -framework GLUT -framework OpenGL
endif

ifdef OPT 
  #turn on optimization
  CXXFLAGS += -O2
else 
  #turn on debugging
  CXXFLAGS += -g
endif

CXX = g++ 

viz2d: viz2d.o ppm.o glsupport.o scenegraph.o picker.o geometry.o material.o renderstates.o texture.o particle_system.h particle_cloud.h imageloader.h imageloader.cpp
	$(LINK.cpp) -o $@ $^ $(LIBS) -lGLEW ${LIB_PATH}/${LIB_NAME_RELEASE}
	install_name_tool -change ./${LIB_NAME_RELEASE} ${LIB_PATH}/${LIB_NAME_RELEASE} viz2d

viz3d: viz3d.o ppm.o glsupport.o
	$(LINK.cpp) -o $@ $^ -lGLEW ${LIB_PATH}/${LIB_NAME_RELEASE}
	install_name_tool -change ./${LIB_NAME_RELEASE} ${LIB_PATH}/${LIB_NAME_RELEASE} viz3d

clean:
	rm -f *.o viz2d viz3d
