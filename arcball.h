#ifndef ARCBALL_H
#define ARCBALL_H

#include "cvec.h"
#include "matrix4.h"

inline Cvec2 getScreenSpaceCoord(const Cvec3& p,
                                 const Matrix4& projection,
                                 double frustNear, double frustFovY,
                                 int screenWidth, int screenHeight) {
  Cvec4 q = projection * Cvec4(p, 1);
  Cvec3 clipCoord = Cvec3(q) / q[3];
  return Cvec2(clipCoord[0] * screenWidth / 2.0 + (screenWidth - 1)/2.0,
               clipCoord[1] * screenHeight / 2.0 + (screenHeight - 1)/2.0);
}

inline double getScreenToEyeScale(double z, double frustFovY, int screenHeight) {
  return -(z * tan(frustFovY * CS175_PI/360.0)) * 2 / screenHeight;
}

#endif

