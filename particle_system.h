#include <fstream>
#include <vector>
#include <map>
#include <utility>
#include <stdlib.h>
#include <vector>
#include <iostream>

#include "cvec.h"

#include <GL/glew.h>
#ifdef __MAC__
#   include <GLUT/glut.h>
#else
#   include <GL/glut.h>
#endif

using namespace std;

class Particle {
private:
    Cvec3 position;
    Cvec3 velocity;
    Cvec3 color;
    
    int age;
    int lifespan;
    bool fade;

public:
    Particle(int life, bool f) {
        lifespan = life;
        age = 0;
        fade = f;
    }

    void setPosition(Cvec3 p) { position = p; }

    void setVelocity(Cvec3 v) { velocity = v; }
    void setColor(Cvec3 c) { color = c; }
    void resetAge() { age = 0; }

    Cvec3 getPosition() { return this->position; }
    Cvec3 getColor() { return color; }

    float getAlpha() {
        if (fade)
            return 1 - (float)age / lifespan;
        else
            return 1;
    }

    // updates the particles position and velocity
    // acc = sum of forces acting on the particle (e.g. gravity, etc.)
    // returns true if the particle is still alive (age < lifespan)
    bool update(Cvec3 acc) {
        position += velocity;
        velocity += acc;
        age++;

        return age < lifespan;
    }
};


class ParticleSystem {
    // point from which particles originate
    Cvec3 source;

    // particles initial direction and speed
    Cvec3 direction;

    // some randomness in particles' initial velocity
    Cvec3 randomness;

    // net forces acting on all particles, in each (xyz) direction
    Cvec3 forces;

    // color of new particles
    Cvec3 color;

    // all particles
    vector<Particle> particles;

    int numParticles;
    int spawnRate;
    int maxParticles;
    int lifespan;
    bool fade;
    
public:
    // rate = constant rate at which particles spawn (per timestep)
    // maxP = maximum particles in existence at any time
    // pLife = lifespan of each particle, in time steps
    ParticleSystem(Cvec3 s, Cvec3 d, int rate, int maxP, int pLife, bool f) {
        source = s;
        direction = d;

        randomness = Cvec3(0.005, 0.005, 0.005);
        forces = Cvec3(0.0, -0.001, 0.0);

        // default color is white
        color = Cvec3(1.0, 1.0, 1.0);
        
        numParticles = 0;
        maxParticles = maxP;
        spawnRate = rate;
        lifespan = pLife;
        fade = f;
    }

    void update() {
        // update existing particles
        for (int i = 0; i < numParticles; i++) {
            updateParticle(&particles[i]);
        }

        // create new particles
        for (int i = 0; i < spawnRate; i++) {
            if (numParticles < maxParticles) {
                Particle p = createNew();
    
                particles.push_back(p);
                numParticles++;
            }
        }
    }
  
    Particle createNew() {
        Particle p(lifespan, fade);

        Cvec3 posVar(0.0, 0.0, 0.0);
        // add small random variation to particle's start position
        for (int i = 0; i < 3; i++) {
            // random number between -1 and 1
            double var = (double)rand() / RAND_MAX;
            var = 2*var - 1.0;

            posVar[i] += var * 0.01;
        }
        p.setPosition(source+posVar);

        Cvec3 v = direction;
        // add small random variation to particle's velocity
        for (int i = 0; i < 3; i++) {
            // random number between -1 and 1
            double var = (double)rand() / RAND_MAX;
            var = 2*var - 1.0;

            v[i] += var * randomness[i];
        }
        p.setVelocity(v);
        
        p.setColor(color);
        
        p.resetAge();
        
        return p;
    }

    void updateParticle(Particle* p) {
        // if particle is older than its lifespan, remove it
        if (!p->update(forces)) {
            // remove the particle
            // Use iterator instead?
            for (int i = 0; i < numParticles; i++) {
                if (&particles[i] == p) {
                    particles.erase(particles.begin() + i);
                    numParticles--;
                }
            }
        }
    }

    vector<Particle> getParticles() {
        return particles;
    }

    void setColor(Cvec3 c) {
        color = c;
    }

    void setRate(int r) {
        spawnRate = r;
    }

    void setLifespan(int l) {
        lifespan = l;
    }

    Cvec3 getDirection() {
        return direction;
    }

    void setDirection(Cvec3 d) {
        direction = d;
    }
};
