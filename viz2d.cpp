////////////////////////////////////////////////////////////////////////
//
//   Harvard University
//   CS175 : Computer Graphics
//   Professor Steven Gortler
//
////////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>
#include <memory>
#include <math.h>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <algorithm>
#if __GNUG__
#   include <tr1/memory>
#endif

#include <GL/glew.h>
#ifdef __MAC__
#   include <GLUT/glut.h>
#else
#   include <GL/glut.h>
#endif

#include "cvec.h"
#include "matrix4.h"
#include "quat.h"
#include "rigtform.h"
#include "arcball.h"
#include "geometrymaker.h"
#include "geometry.h"
#include "ppm.h"
#include "glsupport.h"
#include "asstcommon.h"
#include "scenegraph.h"
#include "drawer.h"
#include "picker.h"
#include "sgutils.h"
#include "string.h"
#include "mesh.h"
#include "particle_system.h"
#include "particle_cloud.h"
#include "imageloader.h"

#include "lib/inc/fmod.hpp"
#include "lib/inc/fmod_errors.h"
#include "wincompat.h"

#define MODE_SKY 0
#define MODE_RED 1
#define MODE_BLUE 2

#define OUTPUTRATE 44100
#define SPECTRUMSIZE 8192
#define SPECTRUMRANGE ((float)OUTPUTRATE / 2.0f)
#define BINSIZE (SPECTRUMRANGE / (float)SPECTRUMSIZE)

void ERRCHECK(FMOD_RESULT result) {
    if (result != FMOD_OK) {
        printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
        exit(-1);
    }   
}

using namespace std;      // for string, vector, iostream, and other standard C++ stuff
using namespace std::tr1; // for shared_ptr

// G L O B A L S ///////////////////////////////////////////////////

// --------- IMPORTANT --------------------------------------------------------
// Before you start working on this assignment, set the following variable
// properly to indicate whether you want to use OpenGL 2.x with GLSL 1.0 or
// OpenGL 3.x+ with GLSL 1.3.
//
// Set g_Gl2Compatible = true to use GLSL 1.0 and g_Gl2Compatible = false to
// use GLSL 1.3. Make sure that your machine supports the version of GLSL you
// are using. In particular, on Mac OS X currently there is no way of using
// OpenGL 3.x with GLSL 1.3 when GLUT is used.
//
// If g_Gl2Compatible=true, shaders with -gl2 suffix will be loaded.
// If g_Gl2Compatible=false, shaders with -gl3 suffix will be loaded.
// To complete the assignment you only need to edit the shader files that get
// loaded
// ----------------------------------------------------------------------------
const bool g_Gl2Compatible = true;


static const float g_frustMinFov = 60.0;  // 60 degree field of view in y direction
static float g_frustFovY = g_frustMinFov;

static const float g_frustNear = -0.1;    // near plane
static const float g_frustFar = -50.0;    // far plane
static const float g_groundY = -2.0;      // y coordinate of the ground
static const float g_groundSize = 10.0;   // half the ground length

static int g_windowWidth = 512;
static int g_windowHeight = 512;
static bool g_mouseClickDown = false;    // is the mouse button pressed
static bool g_mouseLClickButton, g_mouseRClickButton, g_mouseMClickButton;
static int g_mouseClickX, g_mouseClickY; // coordinates for mouse click event
static bool g_simulateMiddleClick = false;

static Cvec3 g_mouseStart;

static int g_activeShader = 0;
static int g_cameraMode = MODE_SKY;
static int g_manipulateMode = MODE_SKY;
static int g_skyWorldSky = true;
static float g_arcballScreenRadius = 1.0;
static float g_arcballScale = 1.0;
static bool g_pickingMode = false;
static int g_currentFrame = -1;
static int g_msBetweenKeyFrames = 2000;
static int g_animateFramesPerSecond = 60;
static int g_timerFramesPerSecond = 60;
static float g_timerSpeed = 1.0;
static bool animating = false;
static bool skipAnimation = false;
static int g_subdivisions = 0;
static bool g_smooth = false;

static const int PICKING_SHADER = 2; // index of the picking shader is g_shaerFiles
static vector<vector<RigTForm> > g_frames;

static Mesh g_mesh;

static shared_ptr<Material> g_redDiffuseMat,
                            g_blueDiffuseMat,
                            g_bumpFloorMat,
                            g_arcballMat,
                            g_pickingMat,
                            g_lightMat,
                            g_meshMat;

shared_ptr<Material> g_overridingMaterial;

static const char* filename = "animation.txt";

// --------- Geometry
typedef SgGeometryShapeNode MyShapeNode;

static shared_ptr<SgRootNode> g_world;
static shared_ptr<SgRbtNode> g_skyNode, g_groundNode, g_robot1Node, g_robot2Node, g_light1Node, g_light2Node, g_meshNode;
static shared_ptr<SgRbtNode> g_currentPickedRbtNode;
static shared_ptr<SgRbtNode>* g_eyeRbtNode = &g_skyNode;

// Vertex buffer and index buffer associated with the ground and cube geometry
static shared_ptr<Geometry> g_ground, g_cube, g_sphere;
static shared_ptr<SimpleGeometryPN> g_meshGeometry;

static const Cvec3 g_light1(2.0, 3.0, 5.0), g_light2(-5.0, 0.0, -8.0);

static vector<ParticleSystem> g_particleFountains;
static vector<ParticleCloud> g_particleClouds;
static shared_ptr<GlTexture> g_tex0;    // global texture instance
static int g_systemType; // 0 = fountain, 1 = cloud
static int g_numTypes = 2;

static FMOD::System* fmod_system;
static FMOD::Sound *sound1, *sound2, *sound3, *sound4, *sound5;
static FMOD::Channel* channel = 0;
static FMOD_RESULT result;
static int left_bin = 0;
static int right_bin = 0;
static float left_hz = 0;
static float right_hz = 0;

///////////////// END OF G L O B A L S //////////////////////////////////////////////////


// Initialize the particle systems
// type determines the kind of particle systems, 0 = fountain, 1 = spring
static void initParticleSystems(int type) {
    ParticleSystem rightFountain(Cvec3(0.5, -0.5, 0.0), Cvec3(0.0, 0.045, 0.0), 3, 8000, 200, true);
    ParticleSystem leftFountain(Cvec3(-0.5, -0.5, 0.0), Cvec3(0.0, 0.045, 0.0), 3, 8000, 200, true);
    rightFountain.setColor(Cvec3(1.0, 0.0, 0.0));
    leftFountain.setColor(Cvec3(0.0, 0.0, 1.0));
    g_particleFountains.push_back(rightFountain);
    g_particleFountains.push_back(leftFountain);

    ParticleCloud cloud(Cvec3(0.0, 0.0, 0.0), 100, Cvec3(0.0, 1.0, 0.0));
    g_particleClouds.push_back(cloud);
}

// takes a projection matrix and send to the the shaders
inline void sendProjectionMatrix(Uniforms& uniforms, const Matrix4& projMatrix) {
    uniforms.put("uProjMatrix", projMatrix);
}

// update g_frustFovY from g_frustMinFov, g_windowWidth, and g_windowHeight
static void updateFrustFovY() {
    if (g_windowWidth >= g_windowHeight)
        g_frustFovY = g_frustMinFov;
    else {
        const double RAD_PER_DEG = 0.5 * CS175_PI/180;
        g_frustFovY = atan2(sin(g_frustMinFov * RAD_PER_DEG) * g_windowHeight / g_windowWidth, cos(g_frustMinFov * RAD_PER_DEG)) / RAD_PER_DEG;
    }
}

static Matrix4 makeProjectionMatrix() {
    return Matrix4::makeProjection(
            g_frustFovY, g_windowWidth / static_cast <double> (g_windowHeight),
            g_frustNear, g_frustFar);
}

static bool compareParticles(Particle p1, Particle p2) {
    return p1.getPosition()[2] < p2.getPosition()[2];
}

static bool compareSpringParticles(SpringParticle p1, SpringParticle p2) {
    return p1.getPosition()[2] < p2.getPosition()[2];
}

static void drawStuff(bool picking) {
    // the FMOD examples/pitchdetection project was referenced to retrieve and analyze the audio spectrum
    fmod_system->update();

    unsigned int ms = 0;
    unsigned int lenms = 0;
    bool playing = 0;
    bool paused = 0;

    if (channel) {
        FMOD::Sound *currentsound = 0;

        result = channel->isPlaying(&playing);
        if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
            ERRCHECK(result);

        result = channel->getPaused(&paused);
        if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
            ERRCHECK(result);

        result = channel->getPosition(&ms, FMOD_TIMEUNIT_MS);
        if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
            ERRCHECK(result);

        channel->getCurrentSound(&currentsound);
        if (currentsound) {
            result = currentsound->getLength(&lenms, FMOD_TIMEUNIT_MS);
            if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
                ERRCHECK(result);
        }

        if (playing) {
            float left_spectrum[SPECTRUMSIZE];
            float right_spectrum[SPECTRUMSIZE];
            float binsize = BINSIZE;

            // get current spectrum
            result = channel->getSpectrum(left_spectrum, SPECTRUMSIZE, 0, FMOD_DSP_FFT_WINDOW_TRIANGLE);
            ERRCHECK(result);
            result = channel->getSpectrum(right_spectrum, SPECTRUMSIZE, 1, FMOD_DSP_FFT_WINDOW_TRIANGLE);
            ERRCHECK(result);

            // determine maximum pitch in spectrum
            float left_max = 0;
            float right_max = 0;
            float left_sum = 0;
            float right_sum = 0;

            for (int i = 0; i < SPECTRUMSIZE; i++) {

                if (left_spectrum[i] > 0.01f && left_spectrum[i] > left_max) {
                    left_max = left_spectrum[i];
                    left_bin = i;
                }
                left_sum += left_spectrum[i];

                if (right_spectrum[i] > 0.01f && right_spectrum[i] > right_max) {
                    right_max = right_spectrum[i];
                    right_bin = i;
                }
                right_sum += right_spectrum[i];
            }

            left_hz = (float)left_bin * BINSIZE;
            right_hz = (float)right_bin * BINSIZE;

            float rate_right = right_hz / 100;
            if (right_hz > 50. && right_hz < 100.)
                rate_right = right_hz / 30.;

            float rate_left = left_hz / 100;
            if (left_hz > 50. && left_hz < 100.)
                rate_left = left_hz / 30.;

            float color1 = (float)(ms / 1000 % 10) / 10.;
            float color2 = (float)(ms / 1000 % 25) / 25.;
            float color3 = (float)(ms / 1000 % 15) / 15.;
            float color4 = (float)(ms / 1000 % 40) / 40.;

            if (g_systemType == 0) {
                g_particleFountains[0].setRate(rate_right);
                g_particleFountains[1].setRate(rate_left);
                
                g_particleFountains[0].setColor(Cvec3(1.0, color1, color2));
                g_particleFountains[1].setColor(Cvec3(color3, color4, 1.0));
            }

            else if (g_systemType == 1) {
                float speed = 0.001 + (rate_right + rate_left) / 10000;
                float scale = 5 + 15 * (right_max + left_max);
                g_particleClouds[0].setSpeed(speed);
                g_particleClouds[0].setScale(scale);
                
                g_particleClouds[0].setColor(Cvec3(color1, 1.0, color2));
            }

            // display music info
            printf("\r%02d:%02d:%02d, %7.1f -> %7.1f Hz., %7.1f -> %7.1f",
                    ms / 1000 / 60, ms / 1000 % 60, ms / 10 % 100,
                    left_hz, ((float)left_bin + 0.99f) * BINSIZE,
                    right_hz, ((float)right_bin + 0.99f) * BINSIZE);
            
            fflush(stdout);
        }
    }

    // radius of particle
    float rad = 0.02;
    
    glLoadIdentity();

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, *g_tex0);

    if (g_systemType == 0) {
        for (vector<ParticleSystem>::iterator it = g_particleFountains.begin(); it != g_particleFountains.end(); ++it) {
            vector<Particle> particles = (*it).getParticles();
            sort(particles.begin(), particles.end(), compareParticles);

            for (int i = 0; i < particles.size(); i++) {
                // set color
                Cvec3 c = particles[i].getColor();
                glColor4f(c[0], c[1], c[2], particles[i].getAlpha());
                Cvec3 p = particles[i].getPosition();
  
                glBegin(GL_TRIANGLE_STRIP);
                    // top right
                    glTexCoord2f(0.0, 1.0);
                    glVertex3f(p[0]+rad, p[1]+rad, p[2]);

                    // top left
                    glTexCoord2f(0.0, 0.0);
                    glVertex3f(p[0]-rad, p[1]+rad, p[2]);

                    // bottom right
                    glTexCoord2f(1.0, 1.0);
                    glVertex3f(p[0]+rad, p[1]-rad, p[2]);

                    // bottom left
                    glTexCoord2f(1.0, 0.0);
                    glVertex3f(p[0]-rad, p[1]-rad, p[2]);
                glEnd();
            }
        }
    }

    else if (g_systemType == 1) {
        for (vector<ParticleCloud>::iterator it = g_particleClouds.begin(); it != g_particleClouds.end(); ++it) {
            vector<SpringParticle> particles = (*it).getParticles();

            sort(particles.begin(), particles.end(), compareSpringParticles);
            
            for (int i = 0; i < particles.size(); i++) {
                // set color
                Cvec3 c = particles[i].getColor();
                glColor4f(c[0], c[1], c[2], 1);

                Cvec3 p = particles[i].getPosition();

                glBegin(GL_TRIANGLE_STRIP);
                    // top right
                    glTexCoord2f(0.0, 1.0);
                    glVertex3f(p[0]+rad, p[1]+rad, p[2]);

                    // top left
                    glTexCoord2f(0.0, 0.0);
                    glVertex3f(p[0]-rad, p[1]+rad, p[2]);

                    // bottom right
                    glTexCoord2f(1.0, 1.0);
                    glVertex3f(p[0]+rad, p[1]-rad, p[2]);

                    // bottom left
                    glTexCoord2f(1.0, 0.0);
                    glVertex3f(p[0]-rad, p[1]-rad, p[2]);
                glEnd();
            }
        }
    }
}

static void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);                   // clear framebuffer color&depth

    drawStuff(false);

    glutSwapBuffers();                                    // show the back buffer (where we rendered stuff)

    checkGlErrors();
}

static void reshape(const int w, const int h) {
    g_windowWidth = w;
    g_windowHeight = h;
    g_arcballScreenRadius = 0.25 * min(g_windowWidth, g_windowHeight);
    glViewport(0, 0, w, h);
    std::cerr << "Size of window is now " << w << "x" << h << std::endl;
    glutPostRedisplay();
    updateFrustFovY();
}

static void pick() {
    // We need to set the clear color to black, for pick rendering.
    // so let's save the clear color
    GLdouble clearColor[4];
    glGetDoublev(GL_COLOR_CLEAR_VALUE, clearColor);

    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    drawStuff(true);

    // Uncomment below and comment out the glutPostRedisplay in mouse(...) call back
    // to see result of the pick rendering pass
    //glutSwapBuffers();

    //Now set back the clear color
    glClearColor(clearColor[0], clearColor[1], clearColor[2], clearColor[3]);

    checkGlErrors();
}

static void timerCallback(int ms) {
    glutPostRedisplay();

    // update particle systems
    if (g_systemType == 0)
        for (vector<ParticleSystem>::iterator it = g_particleFountains.begin(); it != g_particleFountains.end(); ++it)
            (*it).update();
    else if (g_systemType == 1)
        for (vector<ParticleCloud>::iterator it = g_particleClouds.begin(); it != g_particleClouds.end(); ++it)
            (*it).update();

    // continue animation
    glutTimerFunc(g_timerSpeed * (1000 / g_timerFramesPerSecond), timerCallback, 
        ms + 1000 / g_timerFramesPerSecond);
}

static void keyboard(const unsigned char key, const int x, const int y) {
    switch (key) {
        case 27:
            exit(0);                                  // ESC
        case 's':
            glFlush();
            writePpmScreenshot(g_windowWidth, g_windowHeight, "out.ppm");
            break;
        case '<':
            g_systemType = (g_systemType+g_numTypes-1) % g_numTypes;
            break;
        case '>':
            g_systemType = (g_systemType+1) % g_numTypes;
            break;
        case '1':
            result = channel->stop();
            result = fmod_system->playSound(FMOD_CHANNEL_FREE, sound1, 0, &channel);
            ERRCHECK(result);
            break;
        case '2':
            result = channel->stop();
            result = fmod_system->playSound(FMOD_CHANNEL_FREE, sound2, 0, &channel);
            ERRCHECK(result);
            break;
        case '3':
            result = channel->stop();
            result = fmod_system->playSound(FMOD_CHANNEL_FREE, sound3, 0, &channel);
            ERRCHECK(result);
            break;
        case '4':
            result = channel->stop();
            result = fmod_system->playSound(FMOD_CHANNEL_FREE, sound4, 0, &channel);
            ERRCHECK(result);
            break;
        case '5':
            result = channel->stop();
            result = fmod_system->playSound(FMOD_CHANNEL_FREE, sound5, 0, &channel);
            ERRCHECK(result);
            break;
    }

    glutPostRedisplay();
}

static void initGlutState(int argc, char * argv[]) {
    glutInit(&argc, argv);                                  // initialize Glut based on cmd-line args
    glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);  //  RGBA pixel channels and double buffering
    glutInitWindowSize(g_windowWidth, g_windowHeight);      // create a window
    glutCreateWindow("CS175 Final Project");                       // title the window

    glutDisplayFunc(display);                               // display rendering callback
    glutReshapeFunc(reshape);                               // window reshape callback
    glutKeyboardFunc(keyboard);
}

static void initGLState() {
    //glClearColor(128./255., 200./255., 255./255., 0.);
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClearDepth(0.);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glCullFace(GL_BACK);
    
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glDepthFunc(GL_GREATER);
    glReadBuffer(GL_BACK);
    if (!g_Gl2Compatible)
        glEnable(GL_FRAMEBUFFER_SRGB);
}


static void initGeometry() {
    g_systemType = 0;
    initParticleSystems(g_systemType);
}

// this function was inspired by the tutorial at http://www.videotutorialsrock.com/opengl_tutorial/particle_system/text.php
char* addAlphaChannel(Image* image, Image* alphaChannel) {
    char* pixels = new char[image->width * image->height * 4];
    for (int y = 0; y < image->height; y++) {
        for (int x = 0; x < image->width; x++) {
            for (int j = 0; j < 3; j++) {
                pixels[4* (y * image->width + x) + j] = 
                    image->pixels[3 * (y * image->width + x) + j];
            }
            pixels[4* (y * image->width + x) + 3] = 
                alphaChannel->pixels[3 * (y * image->width + x)];
        }
    }

    return pixels;
}

// this function was inspired by the tutorial at http://www.videotutorialsrock.com/opengl_tutorial/particle_system/text.php
static void loadAlphaTexture(GLuint texHandle, Image* image, Image* alpha) {
    char* pixels = addAlphaChannel(image, alpha);

    glBindTexture(GL_TEXTURE_2D, texHandle);
    glTexImage2D(GL_TEXTURE_2D, 0, g_Gl2Compatible ? GL_RGBA : GL_SRGB_ALPHA, image->width, image->height,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    delete pixels;
}

static void initTextures() {
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    g_tex0.reset(new GlTexture());

    Image* image = loadBMP("circle.bmp");
    Image* alpha = loadBMP("circlealpha.bmp");
    loadAlphaTexture(*g_tex0, image, alpha);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, *g_tex0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    //glEnable(GL_TEXTURE_2D);
    delete image;
    delete alpha;
}


int main(int argc, char** argv) {
    // the FMOD examples/playsound project was referenced to load audio files
    int key;
    unsigned int version;

    result = FMOD::System_Create(&fmod_system);
    ERRCHECK(result);

    result = fmod_system->getVersion(&version);
    ERRCHECK(result);

    if (version < FMOD_VERSION) {
        printf("Error! You are using an old version of FMOD %08x. This program requires %08x\n", version, FMOD_VERSION);
        getch();
        return 0;
    }   

    result = fmod_system->init(32, FMOD_INIT_NORMAL, NULL);
    ERRCHECK(result);

    printf("Loading");
    fflush(stdout);

    result = fmod_system->createSound("media/gangnam.mp3", FMOD_SOFTWARE, 0, &sound1);
    ERRCHECK(result);
    printf(".");
    fflush(stdout);

    result = fmod_system->createSound("media/skrillex.mp3", FMOD_SOFTWARE, 0, &sound2);
    ERRCHECK(result);
    printf(".");
    fflush(stdout);

    result = fmod_system->createSound("media/shop.mp3", FMOD_SOFTWARE, 0, &sound3);
    ERRCHECK(result);
    printf(".");
    fflush(stdout);

    result = fmod_system->createSound("media/back.mp3", FMOD_SOFTWARE, 0, &sound4);
    ERRCHECK(result);
    printf(".");
    fflush(stdout);

    result = fmod_system->createSound("media/young.mp3", FMOD_SOFTWARE, 0, &sound5);
    ERRCHECK(result);
    printf(".");
    fflush(stdout);

    printf(" Done!\n\n");
    printf("Pick a song to visualize!\n=========================\n\n");
    printf("1: Gangnam Style - PSY\n");
    printf("2: Scary Monsters and Nice Sprites - Skrillex\n");
    printf("3: Thift Shop - Macklemore ft. Ryan Lewis\n");
    printf("4: Want U Back - Cher Lloyd\n");
    printf("5: Die Young - Ke$ha\n");
    printf("\n");

    try {
        initGlutState(argc, argv);

        glewInit(); // load the OpenGL extensions

        cout << (g_Gl2Compatible ? "Will use OpenGL 2.x / GLSL 1.0" : "Will use OpenGL 3.x / GLSL 1.3") << endl;
        if ((!g_Gl2Compatible) && !GLEW_VERSION_3_0)
            throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.3");
        else if (g_Gl2Compatible && !GLEW_VERSION_2_0)
            throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.0");

        initGLState();
        initTextures();
        initGeometry();

        timerCallback(0);

        glutMainLoop();
        return 0;
    }
    catch (const runtime_error& e) {
        cout << "Exception caught: " << e.what() << endl;
        return -1;
    }
}
