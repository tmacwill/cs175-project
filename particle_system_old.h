#include <fstream>
#include <vector>
#include <map>
#include <utility>
#include <stdlib.h>
#include <vector>

#include "cvec.h"

using namespace std;

class Particle {
    public:
        Particle::Particle(int life) {
            lifetime = life;
            age = 0;
        }

        void setPosition(Cvec3 p);
        void setVelocity(Cvec3 v);
        void setColor(Cvec3 c);
        void resetAge();

        // updates the particles position and velocity
        // acc = sum of forces acting on the particle (e.g. gravity, etc.)
        // returns true if the particle is still alive (age < lifetime)
        bool update(Cvec3 acc);

    private:
        Cvec3 position;
        Cvec3 velocity;
        
        Cvec3 color; // different type?

        // number of timesteps before this particle disappears
        int lifetime;
    
        // number of timesteps this particle has been alive
        int age;
};

class ParticleSystem {
    public:
        // numP = number of particles visible at any time
        // pLife = lifetime of each particle (in timesteps)
        // particle spawn rate = numP / pLife
        ParticleSystem::ParticleSystem(int numP, int pLife);

        void update();
  
        void reset(Particle p);

        void updateParticle(Particle p);

    private:
        // source from which particles spawn
        Cvec3 source;

        // initial direction of particles
        Cvec3 direction;

        // amount of randomness to incorporate in initial direction
        Cvec3 randomness;

        // forces acting on particles produced by this source
        Cvec3 gravity;

        int numParticles;
        vector<Particle> particles;
};
