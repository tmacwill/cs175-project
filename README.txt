Tommy MacWilliam <tmacwilliam@cs.harvard.edu>
Jacob Pritt <mark.pritt@college.harvard.edu>
===

Mac OS X 10.8 was used for development, and our code currently does not work on other platforms. However, the library we are using for music analysis is cross-platform, so our code should be portable to other platforms given the appropriate version of the FMOD library.
To build the 2D system, execute `make viz2d`. It can then be run with `./viz2d`.
To build the 3D system, execute `make viz3d`. It can then be run with `./viz3d`.

First, select a number from the menu to choose a song to visualize. To change songs, simply press another number. In both programs, the '<' and '>' keys can be used to toggle particle system types. In the fountain visualization found in `viz3d`, w, a, s, d can be moved to move the left fountain, and i, j, k, l can be used to move the right fountain.
