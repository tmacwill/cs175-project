////////////////////////////////////////////////////////////////////////
//
//   Harvard University
//   CS175 : Computer Graphics
//   Professor Steven Gortler
//
////////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>
#include <memory>
#include <math.h>
#include <stdexcept>
#if __GNUG__
#   include <tr1/memory>
#endif

#include <GL/glew.h>
#ifdef __MAC__
#   include <GLUT/glut.h>
#else
#   include <GL/glut.h>
#endif

#include "cvec.h"
#include "matrix4.h"
#include "quat.h"
#include "rigtform.h"
#include "arcball.h"
#include "geometrymaker.h"
#include "ppm.h"
#include "glsupport.h"
#include "particle_system.h"
#include "particle_cloud.h"

#include "lib/inc/fmod.hpp"
#include "lib/inc/fmod_errors.h"
#include "wincompat.h"

#define MODE_SKY 0
#define MODE_RED 1
#define MODE_BLUE 2

#define OUTPUTRATE 44100
#define SPECTRUMSIZE 8192
#define SPECTRUMRANGE ((float)OUTPUTRATE / 2.0f)
#define BINSIZE (SPECTRUMRANGE / (float)SPECTRUMSIZE)

void ERRCHECK(FMOD_RESULT result) {
    if (result != FMOD_OK) {
        printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
        exit(-1);
    }   
}

using namespace std;      // for string, vector, iostream, and other standard C++ stuff
using namespace std::tr1; // for shared_ptr

// G L O B A L S ///////////////////////////////////////////////////

// --------- IMPORTANT --------------------------------------------------------
// Before you start working on this assignment, set the following variable
// properly to indicate whether you want to use OpenGL 2.x with GLSL 1.0 or
// OpenGL 3.x+ with GLSL 1.3.
//
// Set g_Gl2Compatible = true to use GLSL 1.0 and g_Gl2Compatible = false to
// use GLSL 1.3. Make sure that your machine supports the version of GLSL you
// are using. In particular, on Mac OS X currently there is no way of using
// OpenGL 3.x with GLSL 1.3 when GLUT is used.
//
// If g_Gl2Compatible=true, shaders with -gl2 suffix will be loaded.
// If g_Gl2Compatible=false, shaders with -gl3 suffix will be loaded.
// To complete the assignment you only need to edit the shader files that get
// loaded
// ----------------------------------------------------------------------------
static const bool g_Gl2Compatible = true;


static const float g_frustMinFov = 60.0;  // 60 degree field of view in y direction
static float g_frustFovY = g_frustMinFov;

static const float g_frustNear = -0.1;    // near plane
static const float g_frustFar = -50.0;    // far plane
static const float g_groundY = -2.0;      // y coordinate of the ground
static const float g_groundSize = 10.0;   // half the ground length

static int g_windowWidth = 512;
static int g_windowHeight = 512;
static bool g_mouseClickDown = false;    // is the mouse button pressed
static bool g_mouseLClickButton, g_mouseRClickButton, g_mouseMClickButton;
static int g_mouseClickX, g_mouseClickY; // coordinates for mouse click event

static Cvec3 g_mouseStart;

static int g_activeShader = 0;
static int g_cameraMode = MODE_SKY;
static int g_manipulateMode = MODE_SKY;
static int g_skyWorldSky = true;
static float g_arcballScreenRadius = 1.0;
static float g_arcballScale = 1.0;

static int g_timerFramesPerSecond = 60;
static float g_timerSpeed = 1.0;

struct ShaderState {
    GlProgram program;

    // Handles to uniform variables
    GLint h_uLight, h_uLight2;
    GLint h_uProjMatrix;
    GLint h_uModelViewMatrix;
    GLint h_uNormalMatrix;
    GLint h_uColor;

    // Handles to vertex attributes
    GLint h_aPosition;
    GLint h_aNormal;

    ShaderState(const char* vsfn, const char* fsfn) {
        readAndCompileShader(program, vsfn, fsfn);

        const GLuint h = program; // short hand

        // Retrieve handles to uniform variables
        h_uLight = safe_glGetUniformLocation(h, "uLight");
        h_uLight2 = safe_glGetUniformLocation(h, "uLight2");
        h_uProjMatrix = safe_glGetUniformLocation(h, "uProjMatrix");
        h_uModelViewMatrix = safe_glGetUniformLocation(h, "uModelViewMatrix");
        h_uNormalMatrix = safe_glGetUniformLocation(h, "uNormalMatrix");
        h_uColor = safe_glGetUniformLocation(h, "uColor");

        // Retrieve handles to vertex attributes
        h_aPosition = safe_glGetAttribLocation(h, "aPosition");
        h_aNormal = safe_glGetAttribLocation(h, "aNormal");

        if (!g_Gl2Compatible)
            glBindFragDataLocation(h, 0, "fragColor");
        checkGlErrors();
    }

};

static const int g_numShaders = 2;
static const char * const g_shaderFiles[g_numShaders][2] = {
    {"./shaders/basic-gl3.vshader", "./shaders/diffuse-gl3.fshader"},
    {"./shaders/basic-gl3.vshader", "./shaders/solid-gl3.fshader"}
};
static const char * const g_shaderFilesGl2[g_numShaders][2] = {
    {"./shaders/basic-gl2.vshader", "./shaders/diffuse-gl2.fshader"},
    {"./shaders/basic-gl2.vshader", "./shaders/solid-gl2.fshader"}
};
static vector<shared_ptr<ShaderState> > g_shaderStates; // our global shader states

// --------- Geometry

// Macro used to obtain relative offset of a field within a struct
#define FIELD_OFFSET(StructType, field) &(((StructType *)0)->field)

// A vertex with floating point position and normal
struct VertexPN {
    Cvec3f p, n;

    VertexPN() {}
    VertexPN(float x, float y, float z,
            float nx, float ny, float nz)
        : p(x,y,z), n(nx, ny, nz)
    {}

    // Define copy constructor and assignment operator from GenericVertex so we can
    // use make* functions from geometrymaker.h
    VertexPN(const GenericVertex& v) {
        *this = v;
    }

    VertexPN& operator = (const GenericVertex& v) {
        p = v.pos;
        n = v.normal;
        return *this;
    }
};

struct Geometry {
    GlBufferObject vbo, ibo;
    int vboLen, iboLen;

    Geometry(VertexPN *vtx, unsigned short *idx, int vboLen, int iboLen) {
        this->vboLen = vboLen;
        this->iboLen = iboLen;

        // Now create the VBO and IBO
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(VertexPN) * vboLen, vtx, GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * iboLen, idx, GL_STATIC_DRAW);
    }

    void draw(const ShaderState& curSS) {
        // Enable the attributes used by our shader
        safe_glEnableVertexAttribArray(curSS.h_aPosition);
        safe_glEnableVertexAttribArray(curSS.h_aNormal);

        // bind vbo
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        safe_glVertexAttribPointer(curSS.h_aPosition, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPN), FIELD_OFFSET(VertexPN, p));
        safe_glVertexAttribPointer(curSS.h_aNormal, 3, GL_FLOAT, GL_FALSE, sizeof(VertexPN), FIELD_OFFSET(VertexPN, n));

        // bind ibo
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

        // draw!
        glDrawElements(GL_TRIANGLES, iboLen, GL_UNSIGNED_SHORT, 0);

        // Disable the attributes used by our shader
        safe_glDisableVertexAttribArray(curSS.h_aPosition);
        safe_glDisableVertexAttribArray(curSS.h_aNormal);
    }
};


// Vertex buffer and index buffer associated with the ground and cube geometry
static shared_ptr<Geometry> g_ground, g_cube, g_cube2, g_sphere;

// --------- Scene

static const Cvec3 g_light1(2.0, 3.0, 14.0), g_light2(-2, -3.0, -5.0);  // define two lights positions in world space
static RigTForm g_skyRbt = RigTForm(Cvec3(0.0, 0.25, 2.0));
static RigTForm g_objectRbt[2] = {RigTForm(Cvec3(-1, 0, 0)), RigTForm(Cvec3(1, 0, 0))};
static Cvec3f g_objectColors[2] = {Cvec3f(1, 0, 0), Cvec3f(0, 0, 1)};
static RigTForm* g_eyeRbt = &g_skyRbt;

static vector<ParticleSystem> g_particleFountains;
static vector<shared_ptr<Geometry> > g_rightFountainParticles;
static vector<shared_ptr<Geometry> > g_leftFountainParticles;
static vector<shared_ptr<Geometry> > g_cloudParticles;
static vector<ParticleCloud> g_particleClouds;
static shared_ptr<GlTexture> g_tex0;    // global texture instance
static int g_systemType; // 0 = fountain, 1 = cloud
static int g_numTypes = 2;

static FMOD::System* fmod_system;
static FMOD::Sound *sound1, *sound2, *sound3, *sound4, *sound5;
static FMOD::Channel* channel = 0;
static FMOD_RESULT result;
static int left_bin = 0;
static int right_bin = 0;
static float left_hz = 0;
static float right_hz = 0;

static int g_totalParticles = 400;
static int g_particleLifetime = 150;
static float g_particleVelocity = 0.050;

static Cvec3 g_rightFountainDirection = Cvec3(0.0, g_particleVelocity, 0.0);
static Cvec3 g_leftFountainDirection = Cvec3(0.0, g_particleVelocity, 0.0);

///////////////// END OF G L O B A L S //////////////////////////////////////////////////

// Initialize the particle systems
// type determines the kind of particle systems, 0 = fountain, 1 = spring
static void initParticleSystems(int type) {
    ParticleSystem rightFountain(Cvec3(0.5, -0.5, 0.0), g_rightFountainDirection, 3, 
        g_totalParticles, g_particleLifetime, true);
    ParticleSystem leftFountain(Cvec3(-0.5, -0.5, 0.0), g_leftFountainDirection, 3, 
        g_totalParticles, g_particleLifetime, true);

    rightFountain.setColor(Cvec3(1.0, 0.0, 0.0));
    leftFountain.setColor(Cvec3(0.0, 0.0, 1.0));
    g_particleFountains.push_back(rightFountain);
    g_particleFountains.push_back(leftFountain);

    int size = 5;
    int ibLen, vbLen;
    getSphereVbIbLen(size, size, vbLen, ibLen);
    for (int i = 0; i < g_totalParticles; i++) {
        for (int j = 0; j < 2; j++) {
            shared_ptr<Geometry> particle;
            vector<VertexPN> vtx3(vbLen);
            vector<unsigned short> idx3(ibLen);

            makeSphere(0.03, size, size, vtx3.begin(), idx3.begin());
            particle.reset(new Geometry(&vtx3[0], &idx3[0], vbLen, ibLen));

            if (!j)
                g_rightFountainParticles.push_back(particle);
            else
                g_leftFountainParticles.push_back(particle);
        }
    }
   
    int cloudParticles = 100;
    ParticleCloud cloud(Cvec3(0.0, 0.0, 0.0), cloudParticles, Cvec3(0.0, 1.0, 0.0));
    g_particleClouds.push_back(cloud);

    for (int i = 0; i < cloudParticles; i++) {
        shared_ptr<Geometry> particle;
        vector<VertexPN> vtx3(vbLen);
        vector<unsigned short> idx3(ibLen);

        makeSphere(0.03, size, size, vtx3.begin(), idx3.begin());
        particle.reset(new Geometry(&vtx3[0], &idx3[0], vbLen, ibLen));
        g_cloudParticles.push_back(particle);
    }
}

static void initGround() {
    // A x-z plane at y = g_groundY of dimension [-g_groundSize, g_groundSize]^2
    VertexPN vtx[4] = {
        VertexPN(-g_groundSize, g_groundY, -g_groundSize, 0, 1, 0),
        VertexPN(-g_groundSize, g_groundY,  g_groundSize, 0, 1, 0),
        VertexPN( g_groundSize, g_groundY,  g_groundSize, 0, 1, 0),
        VertexPN( g_groundSize, g_groundY, -g_groundSize, 0, 1, 0),
    };
    unsigned short idx[] = {0, 1, 2, 0, 2, 3};
    g_ground.reset(new Geometry(&vtx[0], &idx[0], 4, 6));
}

static void initCubes() {
    int ibLen, vbLen;
    getCubeVbIbLen(vbLen, ibLen);

    // Temporary storage for cube geometry
    vector<VertexPN> vtx(vbLen);
    vector<unsigned short> idx(ibLen);

    makeCube(1, vtx.begin(), idx.begin());
    g_cube.reset(new Geometry(&vtx[0], &idx[0], vbLen, ibLen));

    vector<VertexPN> vtx2(vbLen);
    vector<unsigned short> idx2(ibLen);

    makeCube(1, vtx2.begin(), idx2.begin());
    g_cube2.reset(new Geometry(&vtx2[0], &idx2[0], vbLen, ibLen));

    int n = 15;
    getSphereVbIbLen(n, n, vbLen, ibLen);
    vector<VertexPN> vtx3(vbLen);
    vector<unsigned short> idx3(ibLen);

    makeSphere(1, n, n, vtx3.begin(), idx3.begin());
    g_sphere.reset(new Geometry(&vtx3[0], &idx3[0], vbLen, ibLen));
}

// takes a projection matrix and send to the the shaders
static void sendProjectionMatrix(const ShaderState& curSS, const Matrix4& projMatrix) {
    GLfloat glmatrix[16];
    projMatrix.writeToColumnMajorMatrix(glmatrix); // send projection matrix
    safe_glUniformMatrix4fv(curSS.h_uProjMatrix, glmatrix);
}

// takes MVM and its normal matrix to the shaders
static void sendModelViewNormalMatrix(const ShaderState& curSS, const Matrix4& MVM, const Matrix4& NMVM) {
    GLfloat glmatrix[16];
    MVM.writeToColumnMajorMatrix(glmatrix); // send MVM
    safe_glUniformMatrix4fv(curSS.h_uModelViewMatrix, glmatrix);

    NMVM.writeToColumnMajorMatrix(glmatrix); // send NMVM
    safe_glUniformMatrix4fv(curSS.h_uNormalMatrix, glmatrix);
}

// update g_frustFovY from g_frustMinFov, g_windowWidth, and g_windowHeight
static void updateFrustFovY() {
    if (g_windowWidth >= g_windowHeight)
        g_frustFovY = g_frustMinFov;
    else {
        const double RAD_PER_DEG = 0.5 * CS175_PI/180;
        g_frustFovY = atan2(sin(g_frustMinFov * RAD_PER_DEG) * g_windowHeight / g_windowWidth, cos(g_frustMinFov * RAD_PER_DEG)) / RAD_PER_DEG;
    }
}

static Matrix4 makeProjectionMatrix() {
    return Matrix4::makeProjection(
            g_frustFovY, g_windowWidth / static_cast <double> (g_windowHeight),
            g_frustNear, g_frustFar);
}

static bool compareParticles(Particle p1, Particle p2) {
    return p1.getPosition()[2] < p2.getPosition()[2];
}

static bool compareSpringParticles(SpringParticle p1, SpringParticle p2) {
    return p1.getPosition()[2] < p2.getPosition()[2];
}

static void drawStuff() {
    // the FMOD examples/pitchdetection project was referenced to retrieve and analyze the audio spectrum
    fmod_system->update();

    unsigned int ms = 0;
    unsigned int lenms = 0;
    bool playing = 0;
    bool paused = 0;

    if (channel) {
        FMOD::Sound *currentsound = 0;

        result = channel->isPlaying(&playing);
        if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
            ERRCHECK(result);

        result = channel->getPaused(&paused);
        if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
            ERRCHECK(result);

        result = channel->getPosition(&ms, FMOD_TIMEUNIT_MS);
        if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
            ERRCHECK(result);

        channel->getCurrentSound(&currentsound);
        if (currentsound) {
            result = currentsound->getLength(&lenms, FMOD_TIMEUNIT_MS);
            if ((result != FMOD_OK) && (result != FMOD_ERR_INVALID_HANDLE) && (result != FMOD_ERR_CHANNEL_STOLEN))
                ERRCHECK(result);
        }

        if (playing) {
            float left_spectrum[SPECTRUMSIZE];
            float right_spectrum[SPECTRUMSIZE];
            float binsize = BINSIZE;

            // get current spectrum
            result = channel->getSpectrum(left_spectrum, SPECTRUMSIZE, 0, FMOD_DSP_FFT_WINDOW_TRIANGLE);
            ERRCHECK(result);
            result = channel->getSpectrum(right_spectrum, SPECTRUMSIZE, 1, FMOD_DSP_FFT_WINDOW_TRIANGLE);
            ERRCHECK(result);

            // determine maximum pitch in spectrum
            float left_max = 0;
            float right_max = 0;
            for (int i = 0; i < SPECTRUMSIZE; i++) {
                if (left_spectrum[i] > 0.01f && left_spectrum[i] > left_max) {
                    left_max = left_spectrum[i];
                    left_bin = i;
                }

                if (right_spectrum[i] > 0.01f && right_spectrum[i] > right_max) {
                    right_max = right_spectrum[i];
                    right_bin = i;
                }
            }
            left_hz = (float)left_bin * BINSIZE;
            right_hz = (float)right_bin * BINSIZE;

            float rate_right = right_hz / 100;
            if (right_hz > 50. && right_hz < 100.)
                rate_right = right_hz / 30.;

            float rate_left = left_hz / 100;
            if (left_hz > 50. && left_hz < 100.)
                rate_left = left_hz / 30.;


            float color1 = (float)(ms / 1000 % 10) / 10.;
            float color2 = (float)(ms / 1000 % 25) / 25.;
            float color3 = (float)(ms / 1000 % 15) / 15.;
            float color4 = (float)(ms / 1000 % 40) / 40.;

            if (g_systemType == 0) {
                g_particleFountains[0].setRate(rate_right);
                g_particleFountains[1].setRate(rate_left);
                
                g_particleFountains[0].setColor(Cvec3(1.0, color1, color2));
                g_particleFountains[1].setColor(Cvec3(color3, color4, 1.0));
            }
            else if (g_systemType == 1) {
                float speed = 0.001 + (rate_right + rate_left) / 10000;
                float scale = 5 + 15 * (right_max + left_max);
                g_particleClouds[0].setSpeed(speed);
                g_particleClouds[0].setScale(scale);
                
                g_particleClouds[0].setColor(Cvec3(1.0, color1, color2));
            }

            // display music info
            printf("\r%02d:%02d:%02d, %7.1f -> %7.1f Hz., %7.1f -> %7.1f",
                    ms / 1000 / 60, ms / 1000 % 60, ms / 10 % 100,
                    left_hz, ((float)left_bin + 0.99f) * BINSIZE,
                    right_hz, ((float)right_bin + 0.99f) * BINSIZE);
            fflush(stdout);
        }
    }

    // save arcball scale
    if (!g_mouseMClickButton && !(g_mouseRClickButton && g_mouseLClickButton)) {
        // invalid arcball, so fall back to 0.01
        if (g_cameraMode == MODE_RED || g_cameraMode == MODE_BLUE)
            g_arcballScale = 0.01;
        else if (g_manipulateMode == MODE_SKY)
            g_arcballScale = getScreenToEyeScale((inv(*g_eyeRbt) * RigTForm(Cvec3(0, 0, 0))).getTranslation()[2], g_frustFovY, g_windowHeight);
        else if (g_manipulateMode == MODE_RED)
            g_arcballScale = getScreenToEyeScale((inv(*g_eyeRbt) * g_objectRbt[0]).getTranslation()[2], g_frustFovY, g_windowHeight);
        else if (g_manipulateMode == MODE_BLUE)
            g_arcballScale = getScreenToEyeScale((inv(*g_eyeRbt) * g_objectRbt[1]).getTranslation()[2], g_frustFovY, g_windowHeight);
    }

    // short hand for current shader state
    const ShaderState& curSS = *g_shaderStates[g_activeShader];

    // build & send proj. matrix to vshader
    const Matrix4 projmat = makeProjectionMatrix();
    sendProjectionMatrix(curSS, projmat);

    // use the skyRbt as the eyeRbt
    const Matrix4 invEyeRbt = rigTFormToMatrix(inv(*g_eyeRbt));

    const Cvec3 eyeLight1 = Cvec3(invEyeRbt * Cvec4(g_light1, 1)); // g_light1 position in eye coordinates
    const Cvec3 eyeLight2 = Cvec3(invEyeRbt * Cvec4(g_light2, 1)); // g_light2 position in eye coordinates
    safe_glUniform3f(curSS.h_uLight, eyeLight1[0], eyeLight1[1], eyeLight1[2]);
    safe_glUniform3f(curSS.h_uLight2, eyeLight2[0], eyeLight2[1], eyeLight2[2]);

    // draw ground
    // ===========
    //
    const Matrix4 groundRbt = Matrix4();  // identity
    Matrix4 MVM = invEyeRbt * groundRbt;
    Matrix4 NMVM = normalMatrix(MVM);
    sendModelViewNormalMatrix(curSS, MVM, NMVM);
    safe_glUniform3f(curSS.h_uColor, 0.1, 0.95, 0.1); // set color
    g_ground->draw(curSS);

    // draw particles
    if (g_systemType == 0) {
        for (int j = 0; j < 2; j++) {
            vector<Particle> particles = g_particleFountains[j].getParticles();
            sort(particles.begin(), particles.end(), compareParticles);

            for (int i = 0; i < particles.size(); i++) {
                Cvec3 p = particles[i].getPosition();
                Cvec3 c = particles[i].getColor();
                Matrix4 rbt = Matrix4::makeTranslation(p);

                MVM = invEyeRbt * rbt;
                NMVM = normalMatrix(MVM);
                sendModelViewNormalMatrix(curSS, MVM, NMVM);
                safe_glUniform3f(curSS.h_uColor, c[0], c[1], c[2]);

                if (!j)
                    g_rightFountainParticles[i]->draw(curSS);
                else
                    g_leftFountainParticles[i]->draw(curSS);
            }
        }
    }

    else if (g_systemType == 1) {
        for (int j = 0; j < g_particleClouds.size(); j++) {
            vector<SpringParticle> particles = g_particleClouds[j].getParticles();
            sort(particles.begin(), particles.end(), compareSpringParticles);

            for (int i = 0; i < particles.size(); i++) {
                Cvec3 p = particles[i].getPosition();
                Cvec3 c = particles[i].getColor();
                Matrix4 rbt = Matrix4::makeTranslation(p);

                MVM = invEyeRbt * rbt;
                NMVM = normalMatrix(MVM);
                sendModelViewNormalMatrix(curSS, MVM, NMVM);
                safe_glUniform3f(curSS.h_uColor, c[0], c[1], c[2]);

                g_cloudParticles[i]->draw(curSS);
            }
        }
    }
}

static void display() {
    glUseProgram(g_shaderStates[g_activeShader]->program);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);                   // clear framebuffer color&depth

    drawStuff();

    glutSwapBuffers();                                    // show the back buffer (where we rendered stuff)

    checkGlErrors();
}

static void reshape(const int w, const int h) {
    g_windowWidth = w;
    g_windowHeight = h;
    g_arcballScreenRadius = 0.25 * min(g_windowWidth, g_windowHeight);
    glViewport(0, 0, w, h);
    std::cerr << "Size of window is now " << w << "x" << h << std::endl;
    glutPostRedisplay();
    updateFrustFovY();
}

static void motion(const int x, const int y) {
    double dx = x - g_mouseClickX;
    double dy = g_windowHeight - y - 1 - g_mouseClickY;

    RigTForm m;

    // left button down?
    if (g_mouseLClickButton && !g_mouseRClickButton) {
        // check if arcball applies
        if ((g_manipulateMode == MODE_SKY && g_skyWorldSky) ||
                (g_manipulateMode == MODE_RED && g_cameraMode != MODE_RED) ||
                (g_manipulateMode == MODE_BLUE && g_cameraMode != MODE_BLUE)) {

            // compute coordinates of arcball center
            Cvec2 arcballCoords;
            RigTForm invEyeRbt = inv(*g_eyeRbt);
            if (g_manipulateMode == MODE_SKY)
                arcballCoords = getScreenSpaceCoord((invEyeRbt * Cvec3(0, 0, 0)).getTranslation(),
                        makeProjectionMatrix(), g_frustNear, g_frustFovY, g_windowWidth, g_windowHeight);
            else if (g_manipulateMode == MODE_RED)
                arcballCoords = getScreenSpaceCoord((invEyeRbt * g_objectRbt[0]).getTranslation(),
                        makeProjectionMatrix(), g_frustNear, g_frustFovY, g_windowWidth, g_windowHeight);
            else if (g_manipulateMode == MODE_BLUE)
                arcballCoords = getScreenSpaceCoord((invEyeRbt * g_objectRbt[1]).getTranslation(),
                        makeProjectionMatrix(), g_frustNear, g_frustFovY, g_windowWidth, g_windowHeight);

            // make sure the values returned are not #INF or #IND
            // this can happen when the camera is in the same plane as the arcball, looking perpendicular to it
            if (arcballCoords[0] == arcballCoords[0] && arcballCoords[1] == arcballCoords[1]) {
                // compute mouse's position on sphere
                double sphereX = x - arcballCoords[0];
                double sphereY = (g_windowHeight - y - 1) - arcballCoords[1];
                double sphereZ = g_arcballScreenRadius * g_arcballScreenRadius - sphereX * sphereX - sphereY * sphereY;

                // if mouse is off sphere, then clamp to edge
                Cvec3 sphereCoords;
                if (sphereZ < 0)
                    sphereCoords = Cvec3(sphereX, sphereY, 0);
                else
                    sphereCoords = Cvec3(sphereX, sphereY, sqrt(sphereZ));
                sphereCoords.normalize();

                // create transform if mouse coordinates are valid
                if (g_mouseStart[0] != 0 || g_mouseStart[1] != 0 || g_mouseStart[2] != 0)
                    m = RigTForm(Quat(0, sphereCoords[0], sphereCoords[1], sphereCoords[2]) * Quat(0, -g_mouseStart[0], -g_mouseStart[1], -g_mouseStart[2]));

                g_mouseStart = sphereCoords;
            }
        }

        // arcball does not apply
        else {
            if ((g_cameraMode == MODE_SKY && g_manipulateMode == MODE_SKY) ||
                    (g_cameraMode == MODE_RED && g_manipulateMode == MODE_RED) ||
                    (g_cameraMode == MODE_BLUE && g_manipulateMode == MODE_BLUE)) {
                dy = -dy;
                dx = -dx;
            }   

            m = RigTForm(Quat::makeXRotation(-dy) * Quat::makeYRotation(dx));
        }
    }

    // right button down?
    else if (g_mouseRClickButton && !g_mouseLClickButton) {
        if (g_cameraMode == MODE_SKY && g_manipulateMode == MODE_SKY && g_skyWorldSky) {
            dy = -dy;
            dx = -dx;
        }

        m = RigTForm(Cvec3(dx, dy, 0) * g_arcballScale);
    }

    // middle or (left and right) button down?
    else if (g_mouseMClickButton || (g_mouseLClickButton && g_mouseRClickButton)) {
        if (g_cameraMode == MODE_SKY && g_manipulateMode == MODE_SKY && g_skyWorldSky) {
            dy = -dy;
            dx = -dx;
        }

        m = RigTForm(Cvec3(0, 0, -dy) * g_arcballScale);
    }

    if (g_mouseClickDown) {
        // sky cam manipulating cube
        if (g_cameraMode == MODE_SKY && (g_manipulateMode == MODE_RED || g_manipulateMode == MODE_BLUE)) {
            if (g_manipulateMode == MODE_RED)
                g_objectRbt[0] = doMtoOwrtA(m, g_objectRbt[0], transFact(g_objectRbt[0]) * linFact(g_skyRbt));
            else if (g_manipulateMode == MODE_BLUE)
                g_objectRbt[1] = doMtoOwrtA(m, g_objectRbt[1], transFact(g_objectRbt[1]) * linFact(g_skyRbt));
        }

        // looking from sky manipulating sky
        else if (g_cameraMode == MODE_SKY && g_manipulateMode == MODE_SKY) {
            if (g_skyWorldSky) {
                // invert sign of rotation so behavior matches solution
                Quat q = m.getRotation();
                Cvec3 t = m.getTranslation();
                m = RigTForm(t, Quat(-q[0], q[1], q[2], q[3]));

                g_skyRbt = doMtoOwrtA(m, g_skyRbt, 
                        transFact(RigTForm(Cvec3(0, 0, 0))) * linFact(g_skyRbt));
            }
            else
                g_skyRbt = doMtoOwrtA(m, g_skyRbt, transFact(g_skyRbt) * linFact(g_skyRbt));
        }

        // looking from red manipulating red
        else if (g_cameraMode == MODE_RED && g_manipulateMode == MODE_RED)
            g_objectRbt[0] = g_objectRbt[0] * m;

        // looking from blue manipulating blue
        else if (g_cameraMode == MODE_BLUE && g_manipulateMode == MODE_BLUE)
            g_objectRbt[1] = g_objectRbt[1] * m;

        // looking from red manipulating blue
        else if (g_cameraMode == MODE_RED && g_manipulateMode == MODE_BLUE)
            g_objectRbt[1] = doMtoOwrtA(m, g_objectRbt[1], transFact(g_objectRbt[1]) * linFact(g_objectRbt[0]));

        // looking from blue manipulating red
        else if (g_cameraMode == MODE_BLUE && g_manipulateMode == MODE_RED)
            g_objectRbt[0] = doMtoOwrtA(m, g_objectRbt[0], transFact(g_objectRbt[0]) * linFact(g_objectRbt[1]));

        glutPostRedisplay(); // we always redraw if we changed the scene
    }

    g_mouseClickX = x;
    g_mouseClickY = g_windowHeight - y - 1;
}


static void mouse(const int button, const int state, const int x, const int y) {
    g_mouseClickX = x;
    g_mouseClickY = g_windowHeight - y - 1;  // conversion from GLUT window-coordinate-system to OpenGL window-coordinate-system

    g_mouseLClickButton |= (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN);
    g_mouseRClickButton |= (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN);
    g_mouseMClickButton |= (button == GLUT_MIDDLE_BUTTON && state == GLUT_DOWN);

    g_mouseLClickButton &= !(button == GLUT_LEFT_BUTTON && state == GLUT_UP);
    g_mouseRClickButton &= !(button == GLUT_RIGHT_BUTTON && state == GLUT_UP);
    g_mouseMClickButton &= !(button == GLUT_MIDDLE_BUTTON && state == GLUT_UP);

    g_mouseClickDown = g_mouseLClickButton || g_mouseRClickButton || g_mouseMClickButton;

    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
        g_mouseStart = Cvec3(0,0,0);

    glutPostRedisplay();
}

static void timerCallback(int ms) {
    glutPostRedisplay();

    // update particle systems
    if (g_systemType == 0)
        for (vector<ParticleSystem>::iterator it = g_particleFountains.begin(); it != g_particleFountains.end(); ++it)
            (*it).update();
    else if (g_systemType == 1)
        for (vector<ParticleCloud>::iterator it = g_particleClouds.begin(); it != g_particleClouds.end(); ++it)
            (*it).update();

    // continue animation
    glutTimerFunc(g_timerSpeed * (1000 / g_timerFramesPerSecond), timerCallback, 
        ms + 1000 / g_timerFramesPerSecond);
}

static void keyboard(const unsigned char key, const int x, const int y) {
    Cvec3 d;

    switch (key) {
        case 27:
            exit(0);                                  // ESC
        case 't':
            glFlush();
            writePpmScreenshot(g_windowWidth, g_windowHeight, "out.ppm");
            break;
        case '<':
            g_systemType = (g_systemType+g_numTypes-1) % g_numTypes;
            break;
        case '>':
            g_systemType = (g_systemType+1) % g_numTypes;
            break;
        case '1':
            result = channel->stop();
            result = fmod_system->playSound(FMOD_CHANNEL_FREE, sound1, 0, &channel);
            ERRCHECK(result);
            break;
        case '2':
            result = channel->stop();
            result = fmod_system->playSound(FMOD_CHANNEL_FREE, sound2, 0, &channel);
            ERRCHECK(result);
            break;
        case '3':
            result = channel->stop();
            result = fmod_system->playSound(FMOD_CHANNEL_FREE, sound3, 0, &channel);
            ERRCHECK(result);
            break;
        case '4':
            result = channel->stop();
            result = fmod_system->playSound(FMOD_CHANNEL_FREE, sound4, 0, &channel);
            ERRCHECK(result);
            break;
        case '5':
            result = channel->stop();
            result = fmod_system->playSound(FMOD_CHANNEL_FREE, sound5, 0, &channel);
            ERRCHECK(result);
            break;
        case 'a':
            d = g_particleFountains[1].getDirection();
            d[0] -= 0.008;
            g_particleFountains[1].setDirection(d);
            break;
        case 'd':
            d = g_particleFountains[1].getDirection();
            d[0] += 0.008;
            g_particleFountains[1].setDirection(d);
            break;
        case 'w':
            d = g_particleFountains[1].getDirection();
            d[2] -= 0.008;
            g_particleFountains[1].setDirection(d);
            break;
        case 's':
            d = g_particleFountains[1].getDirection();
            d[2] += 0.008;
            g_particleFountains[1].setDirection(d);
            break;
        case 'j':
            d = g_particleFountains[0].getDirection();
            d[0] -= 0.008;
            g_particleFountains[0].setDirection(d);
            break;
        case 'l':
            d = g_particleFountains[0].getDirection();
            d[0] += 0.008;
            g_particleFountains[0].setDirection(d);
            break;
        case 'i':
            d = g_particleFountains[0].getDirection();
            d[2] -= 0.008;
            g_particleFountains[0].setDirection(d);
            break;
        case 'k':
            d = g_particleFountains[0].getDirection();
            d[2] += 0.008;
            g_particleFountains[0].setDirection(d);
            break;
    }

    glutPostRedisplay();
}

static void initGlutState(int argc, char * argv[]) {
    glutInit(&argc, argv);                                  // initialize Glut based on cmd-line args
    glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);  //  RGBA pixel channels and double buffering
    glutInitWindowSize(g_windowWidth, g_windowHeight);      // create a window
    glutCreateWindow("CS175 Final Project");                       // title the window

    glutDisplayFunc(display);                               // display rendering callback
    glutReshapeFunc(reshape);                               // window reshape callback
    glutMotionFunc(motion);                                 // mouse movement callback
    glutMouseFunc(mouse);                                   // mouse click callback
    glutKeyboardFunc(keyboard);
}

static void initGLState() {
    glClearColor(128./255., 200./255., 255./255., 0.);
    glClearDepth(0.);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_GREATER);
    glReadBuffer(GL_BACK);
    if (!g_Gl2Compatible)
        glEnable(GL_FRAMEBUFFER_SRGB);
}

static void initShaders() {
    g_shaderStates.resize(g_numShaders);
    for (int i = 0; i < g_numShaders; ++i) {
        if (g_Gl2Compatible)
            g_shaderStates[i].reset(new ShaderState(g_shaderFilesGl2[i][0], g_shaderFilesGl2[i][1]));
        else
            g_shaderStates[i].reset(new ShaderState(g_shaderFiles[i][0], g_shaderFiles[i][1]));
    }
}

static void initGeometry() {
    initGround();
    initCubes();

    g_systemType = 0;
    initParticleSystems(g_systemType);
}

int main(int argc, char** argv) {
    // the FMOD examples/playsound project was referenced to load audio files
    int key;
    unsigned int version;

    result = FMOD::System_Create(&fmod_system);
    ERRCHECK(result);

    result = fmod_system->getVersion(&version);
    ERRCHECK(result);

    if (version < FMOD_VERSION) {
        printf("Error! You are using an old version of FMOD %08x. This program requires %08x\n", version, FMOD_VERSION);
        getch();
        return 0;
    }   

    result = fmod_system->init(32, FMOD_INIT_NORMAL, NULL);
    ERRCHECK(result);

    printf("Loading");
    fflush(stdout);

    result = fmod_system->createSound("media/gangnam.mp3", FMOD_SOFTWARE, 0, &sound1);
    ERRCHECK(result);
    printf(".");
    fflush(stdout);

    result = fmod_system->createSound("media/skrillex.mp3", FMOD_SOFTWARE, 0, &sound2);
    ERRCHECK(result);
    printf(".");
    fflush(stdout);

    result = fmod_system->createSound("media/shop.mp3", FMOD_SOFTWARE, 0, &sound3);
    ERRCHECK(result);
    printf(".");
    fflush(stdout);

    result = fmod_system->createSound("media/back.mp3", FMOD_SOFTWARE, 0, &sound4);
    ERRCHECK(result);
    printf(".");
    fflush(stdout);

    result = fmod_system->createSound("media/young.mp3", FMOD_SOFTWARE, 0, &sound5);
    ERRCHECK(result);
    printf(".");
    fflush(stdout);

    printf(" Done!\n\n");
    printf("Pick a song to visualize!\n=========================\n\n");
    printf("1: Gangnam Style - PSY\n");
    printf("2: Scary Monsters and Nice Sprites - Skrillex\n");
    printf("3: Thift Shop - Macklemore ft. Ryan Lewis\n");
    printf("4: Want U Back - Cher Lloyd\n");
    printf("5: Die Young - Ke$ha\n");
    printf("\n");

    try {
        initGlutState(argc,argv);

        glewInit(); // load the OpenGL extensions

        cout << (g_Gl2Compatible ? "Will use OpenGL 2.x / GLSL 1.0" : "Will use OpenGL 3.x / GLSL 1.3") << endl;
        if ((!g_Gl2Compatible) && !GLEW_VERSION_3_0)
            throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.3");
        else if (g_Gl2Compatible && !GLEW_VERSION_2_0)
            throw runtime_error("Error: card/driver does not support OpenGL Shading Language v1.0");

        initGLState();
        initShaders();
        initGeometry();

        timerCallback(0);

        glutMainLoop();
        return 0;
    }
    catch (const runtime_error& e) {
        cout << "Exception caught: " << e.what() << endl;
        return -1;
    }
}
