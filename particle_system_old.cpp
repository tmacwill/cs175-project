#include <fstream>
#include <vector>
#include <map>
#include <utility>
#include <stdlib.h>
#include <vector>

#include "cvec.h"
#include "particle_system.h"

using namespace std;


Particle::Particle(int life) {
    lifetime = life;
    age = 0;
}

void Particle::setPosition(Cvec3 p) { position = p; }
void Particle::setVelocity(Cvec3 v) { velocity = v; }
void Particle::setColor(Cvec3 c) { color = c; }
void Particle::resetAge() { age = 0; }

// updates the particles position and velocity
// acc = sum of forces acting on the particle (e.g. gravity, etc.)
// returns true if the particle is still alive (age < lifetime)
bool Particle::update(Cvec3 acc) {
    position += velocity;
    velocity += acc;
    age++;

    return age < lifetime;
}



// numP = number of particles visible at any time
// pLife = lifetime of each particle (in timesteps)
// particle spawn rate = numP / pLife
ParticleSystem::ParticleSystem(int numP, int pLife) {
    randomness = (0.1, 0.1, 0.1);
    gravity = (0, 0, -0.5);
    numParticles = numP;

    for (int i = 0; i < numParticles; i++) {
        Particle p = new Particle(pLife);
           
        reset(p);

        particles.push_back(&p);
    }
}

void ParticleSystem::update() {
    for (int i = 0; i < numParticles; i++) {
        update(particles[i]);
    }
}
  
void ParticleSystem::reset(Particle p) {
    p.setPosition(source);
    Cvec3 v = direction;

    // add small random variation to particle's velocity
    for (int i = 0; i < 3; i++) {
        // random number between -1 and 1
        float rand = 2*(float)rand() / (RAND_MAX+1) - 1.0;

        v[i] += rand * randomness[i];
    }
        
    p.setVelocity(v);
    p.resetAge();
}

void ParticleSystem::updateParticle(Particle p) {
    if (!p.update()) {
        reset(p);
    }
}

